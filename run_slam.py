from asyncio.log import logger
from distutils.log import debug
import numpy as np
import matplotlib.pyplot as plt
import os

# from icp_slam_simple import SLAM
from icp_slam_simple_map_optim import SLAM

from utils import rot_matrix

def plot_map(input_cones, est_cones, input_positions, est_positions):
    plt.figure()
    plt.scatter(input_cones[0,:], input_cones[1,:], s = 8, c='black', marker='.')
    plt.scatter(est_cones[0,:], est_cones[1,:], s = 12, c='red', marker='.')
    plt.scatter(input_positions[0,:], input_positions[1,:], s = 15, c='blue', marker='x')
    plt.scatter(est_positions[0,:], est_positions[1,:], s = 15, c='green', marker='+')
    plt.legend(["input cones", "estimated map", "input positions", "estimated positions"])
    plt.axis('square')


if __name__ == "__main__":
    # pcl_dir = "data/man1/"
    pcl_dir = "data/man2/"
    # pcl_dir = "data/gen_circular_path/"
    # pcl_dir = "data/gen_track1_filter/"
    # pcl_dir = "data/gen_track2/"

    plt.set_loglevel("critical")

    slam_instance = SLAM(debug=True, visualize=True)
    # slam_instance = SLAM(debug=True, visualize=False)

    start = 800
    step = 1
    end = int(len(os.listdir(pcl_dir))/2) - 500

    # start = 1
    # step = 1
    # end = int(len(os.listdir(pcl_dir))/2)

    pc = np.load(pcl_dir + f"points{start:04d}.npy")
    pos = np.load(pcl_dir + f"position{start:04d}.npy")  

    phi_init = pos[2]
    t_init = pos[:2, np.newaxis]

    R_init = rot_matrix(phi_init)
    pc = pc[:2, :]

    slam_instance.process_sample(pc, R_init, t_init)

    pc = np.matmul(R_init, pc) + t_init

    input_cones = pc
    input_positions = t_init
    est_positions = t_init


    for i in range(start+step, end, step):
        pc = np.load(pcl_dir + f"points{i:04d}.npy")
        pos = np.load(pcl_dir + f"position{i:04d}.npy") 

        phi_init = pos[2]
        t_init = pos[:2, np.newaxis]

        R_init = rot_matrix(phi_init)
        pc = pc[:2, :]

        [R_est, t_est] = slam_instance.process_sample(pc, R_init, t_init)
        pc_t = np.matmul(R_est, pc) + t_est
        pc = np.matmul(R_init, pc) + t_init

        input_cones = np.hstack([input_cones, pc])
        input_positions = np.hstack([input_positions, t_init])
        est_positions = np.hstack([est_positions, t_est])
    

    # for t in [5, 10, 20, 50, 100]:
    #     t = 50
    #     print(f"map threshold: {t}")
    #     est_cones = slam_instance.get_map(filtered=True, threshold=t)
    #     plt.show()


    t = 50
    est_cones = slam_instance.get_map(filtered=True, threshold=t)
    plt.savefig("map_optim.png")

    