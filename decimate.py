from shutil import copyfile

src_dir = "data/gen_track1/"
dst_dir = "data/gen_track1_filter/"

new = 1
for old in range(10, 5328, 10):
    copyfile(f"{src_dir}points{old:04d}.npy", f"{dst_dir}points{new:04d}.npy")
    copyfile(f"{src_dir}position{old:04d}.npy", f"{dst_dir}position{new:04d}.npy")
    new += 1    