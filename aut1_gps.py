import numpy as np
import matplotlib.pyplot as plt
import json
import math


def get_heading(o):
    yaw = np.arctan2(
        2.0*(o[1]*o[0] + o[2]*o[3]),
        1 - 2.0*(o[3]*o[3] + o[0]*o[0])
    )

    return yaw + np.pi


def pi_2_pi(angle):
    return (angle + math.pi) % (2 * math.pi) - math.pi


def lidar_to_global(cones, x, y, heading):
    cones = 1 * cones.copy()
    #cones[:, [0, 1]] = cones[:, [1,0]]
    #cones[:, 1] *= -1

    R = np.float32([
        [np.cos(heading), -np.sin(heading)],
        [np.sin(heading), np.cos(heading)]
    ])

    cones = np.matmul(R, cones.T).T
    cones[:, 0] += x
    cones[:, 1] += y

    return cones

def save_npy(data, res_dir):
    for i in range(len(data)):
        x, y, _ = data[i]['position']
        o = data[i]['orientation']

        theta = get_heading(o)
        theta = pi_2_pi(theta)

        position = np.array([x, y, theta])

        cones = np.array(data[i]["detections"])
        cones = cones[:, :2].T

        np.save(res_dir + f"points{i:04d}.npy", cones)
        np.save(res_dir + f"position{i:04d}.npy", position)


def process(data):
    odom = []
    measurements = []
    colors = []

    for i in range(len(data)):
        x, y, _ = data[i]['position']
        o = data[i]['orientation']

        theta = get_heading(o)
        theta = pi_2_pi(theta)

        odom.append([x, y, theta])

        cones = np.array(data[i]["detections"])
        if cones.size > 0:
            colors.append(cones[:, 3])
            cones = lidar_to_global(cones[:, :2], x, y, theta)
            measurements.append(cones)
        else:
            measurements.append([])
            colors.append([])

    odom = np.array(odom)

    return odom, measurements, colors


if __name__ == "__main__":
    with open("slam.json") as f:
        slam = json.load(f)

    # res_dir = "data/man2/"
    # save_npy(slam, res_dir)
    
    odom, measurements, colors = process(slam)

    fig, ax = plt.subplots(1, 2)

    ax[0].scatter(odom[:, 0], odom[:, 1], s=1, c="red")

    for p in odom[800:2000:10]:
        ax[0].arrow(p[0], p[1], 2*np.cos(p[2]), 2*np.sin(p[2]), color="green", width=0.1)

    for m, c in zip(measurements[200:2000:10], colors[200:2000:10]):
        blue = m[c == 0]
        ax[0].scatter(blue[:, 0], blue[:, 1], s=1, c="blue")
        yellow = m[c == 1]
        ax[0].scatter(yellow[:, 0], yellow[:, 1], s=1, c="yellow")


    ax[0].axis('equal')
    ax[1].scatter(np.arange(len(odom)), odom[:, 2], s=1)


    plt.show()
