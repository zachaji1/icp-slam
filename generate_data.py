import numpy as np
import os
import matplotlib.pyplot as plt
import time

def pi_2_pi(angle):
    return (angle + np.pi) % (2 * np.pi) - np.pi

def sparse_cones_model(n_points, xy_min, xy_max):
    model = xy_min + np.random.rand(2, n_points) * (xy_max - xy_min)
    
    return model

def corner_model(n_points):
    p1 = [10, 0]
    p2 = [0, 0]
    p3 = [0, 10]

    corner_x = np.hstack([np.linspace(p1[0], p2[0], round(n_points/2)), np.linspace(p2[0], p3[0], round(n_points/2))]) 
    corner_y = np.hstack([np.linspace(p1[1], p2[1], round(n_points/2)), np.linspace(p2[1], p3[1], round(n_points/2))]) 
    corner = np.vstack([corner_x, corner_y])

    return corner


def transform(points, phi, t):
    points = np.vstack([points, np.ones(points.shape[1])])
    T_matrix = np.array([[np.cos(phi), -np.sin(phi), t[0]], [np.sin(phi), np.cos(phi), t[1]]])
    transformed_points = np.matmul(T_matrix, points)

    return transformed_points

def generate_datapoints(model, n_points, n_copies, res_dir, add_noise=False):   
    if not os.path.exists(res_dir):
        os.mkdir(res_dir)

    model_len = model.shape[1]
    for i in range(n_copies):
        curr_n_points = np.random.randint(round(3/4*n_points), n_points+1)
        indices = np.random.choice(model_len, curr_n_points, replace=False)
        phi = 2*np.pi*np.random.sample()
        t = 20*np.pi*np.random.sample(2)-10
        position = np.hstack([t, phi])
        points = model[:, indices]
        t_points = transform(points, phi, t)
        time.sleep(1)

        np.save(res_dir + f"points{i:04d}.npy", t_points)
        np.save(res_dir + f"position{i:04d}.npy", position)

def circ_path(n_points, radius, center):
    angle = np.linspace(0,2*np.pi, n_points)
    angle = angle[:, np.newaxis]
    x = center[0] + radius * np.cos(angle)
    y = center[1] + radius * np.sin(angle)

    angle[angle > np.pi] = angle[angle > np.pi] - 2*np.pi
    path = np.hstack([x, y, angle])

    return path

def generate_datapoints_path(model, path, fov, max_dist, res_dir):   
    if not os.path.exists(res_dir):
        os.mkdir(res_dir)

    for i, position in enumerate(path):
        t = position[:2]
        phi = position[2]
        
        dist = np.sqrt(np.sum((model - t[:,np.newaxis])**2, 0))
        
        angle = np.arctan2(model[0,:] - t[0], model[1,:] - t[1]) + phi
        angle = pi_2_pi(angle)        
        # angle[angle > np.pi] = angle[angle > np.pi] - 2*np.pi
        
        indices = (dist < max_dist) * (angle >= fov[0]) * (angle <= fov[1])
        # print(sum(indices))
        points = model[:, indices]

        t_points = transform(points, 0, -t)
        t_points = transform(t_points, -phi, [0,0])

        np.save(res_dir + f"points{i:04d}.npy", t_points)
        np.save(res_dir + f"position{i:04d}.npy", position)

if __name__ == "__main__":
    # model = corner_model(200)
    # dst_dir = "data/gen_corner/"
    # generate_datapoints(model, 50, 100, dst_dir, True)
    
    # model = sparse_cones_model(80, -20, 20)
    # dst_dir = "data/gen_circular_path/"
    # path = circ_path(100, 10, np.array([0, 0]))

    model = np.load("data/gen_track1/all_cones.npy")
    dst_dir = "data/gen_track1/"
    path = np.load("data/gen_track1/path.npy")
    path[:, 2] -= np.pi/2

    generate_datapoints_path(model, path, [-np.pi/2, np.pi/2], 20, dst_dir)

    # fig = plt.figure()    
    # plt.ion()
    # plt.show()

    # for i in range(path.shape[0]):
    #     pc = np.load(dst_dir + f"points{i:04d}.npy")
    #     pos = np.load(dst_dir + f"position{i:04d}.npy")
    #     axes = 2*np.array([[1, 0, 0], [0, 0, 1]])
    #     axes_t = transform(axes, pos[2], pos[:2])
        
    #     pc_t = transform(pc, pos[2], pos[:2])

    #     plt.clf()
    #     plt.scatter(model[0, :], model[1, :], c='black') 
    #     plt.scatter(pc[0,:], pc[1,:], marker='x',  c = 'green')
    #     plt.scatter(pc_t[0,:], pc_t[1,:], marker='x',  c = 'blue')
    #     plt.plot(axes_t[0,:], axes_t[1,:])
    #     plt.pause(0.0001)
    #     plt.axis('square')
    #     plt.draw()
    #     time.sleep(0.2)

    # plt.show()