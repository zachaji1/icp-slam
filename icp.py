import numpy as np
from scipy.spatial.distance import cdist
import matplotlib.pyplot as plt
import os
from copy import deepcopy

from utils import rot_matrix

def match_points(points1, points2, outlier_thr):
    # points1 = points1[:, :, np.newaxis]
    # points2 = points2[:, np.newaxis, :]
    # d = np.sqrt(np.sum((points1 - points2)**2, 0))

    d_arr = cdist(points1.T, points2.T, 'euclidean')
    c1 = d_arr.argmin(0)
    c2 = d_arr.argmin(1)
    d = d_arr.min(0)
    is_mutual = c2[c1] == np.arange(0, len(c1))
    c2 = np.arange(0, len(c1))[is_mutual]   
    c1 = c1[is_mutual]
    
    d = d[is_mutual]
    print(d)
    # print(d_arr[c1, c2])
    # is_inlier = d_arr[c1, c2] < outlier_thr
    is_inlier = d < outlier_thr
    
    c2 = c2[is_inlier]
    c1 = c1[is_inlier]

    return c1, c2, d.mean()

def estimate_transform(p_orig, p_new):
    mu_orig = p_orig.mean(1, keepdims=True)
    mu_new = p_new.mean(1, keepdims=True)
    W = np.matmul(p_orig - mu_orig, (p_new - mu_new).T)
    [U, _, V] = np.linalg.svd(W)
    R = np.matmul(U, V) # V.T ?
    if np.linalg.det(R) < 0:
        R = np.matmul(np.matmul(U, np.diag([1, -1])), V)
    t = mu_orig - np.matmul(R, mu_new)

    return R, t
        
def icp(new_points, map, R_init, t_init, outlier_thr = 2, max_iter = 20, min_matches = 2):
    R = deepcopy(R_init)
    t = deepcopy(t_init)
    d_mean_min = np.inf

    if (new_points.shape[1] == 0):
        return (R, t, [], ([], []))


    i = 0
    p_prev = new_points
    while (i < max_iter):      
        p_transformed = np.matmul(R, new_points) + t
        [c1, c2, d_mean] = match_points(map, p_transformed, outlier_thr)
        
        if (len(c1) < min_matches):
            break   # zvysit outlier threshold? 

        p_map = map[:, c1]
        p_new = p_transformed[:, c2]
        [R_est, t_est] = estimate_transform(p_map, p_new)
        R_new = np.matmul(R, R_est)
        t_new = t + t_est
        i += 1

    add_points = [x not in c2 for x in range(new_points.shape[1])]

    if len(c1) < min_matches:
        R = R_init
        t = t_init
        add_points = new_points.shape[1] * [False]

    return (R, t, add_points, (c1, c2))

def update_map(map, map_use_counts, points, add_points, corrs):
    (c1, c2) = corrs
    new_points = points[:, add_points]
    map = np.hstack([map, new_points])
    map_use_counts = np.append(map_use_counts, np.ones(sum(add_points)))
    seen_points = points[:, c2]
    seen_use_counts = map_use_counts[c1]
    map[:, c1] = (seen_use_counts * map[:, c1] + seen_points) / (seen_use_counts + 1)
    map_use_counts[c1] += 1

    return map, map_use_counts

#########################################################################################

if __name__ == "__main__":
    # pcl_dir = "data/man1/"
    pcl_dir = "data/man2/"
    # pcl_dir = "data/gen_circular_path/"
    # pcl_dir = "data/gen_track1/"


    start = 800
    step = 1
    end = int(len(os.listdir(pcl_dir))/2)
    
    # t_sigma = 0.2
    # phi_sigma = 0.01
    # pc_sigma = 0.05

    t_sigma = 0
    phi_sigma = 0
    pc_sigma = 0

    pc = np.load(pcl_dir + f"points{start:04d}.npy")
    pos = np.load(pcl_dir + f"position{start:04d}.npy")  

    phi_init = pos[2] + phi_sigma * np.random.randn()
    t_init = pos[:2, np.newaxis] + t_sigma * np.random.randn(2,1)
    pc = pc + pc_sigma * np.random.rand(pc.shape[0], pc.shape[1])

    R_init = rot_matrix(phi_init)
    pc = pc[:2, :]

    map = np.matmul(R_init, pc) + t_init
    map = map[:2,:]
    map_use_counts = np.ones(map.shape[1])

    axes = np.array([[1, 0, 0], [0, 0, 1]])
    axes_t = np.matmul(R_init, axes) + t_init
    prev_positions = [axes_t]

    plt.ion()
    plt.show()

    for i in range(start+step, end, step):
        pc = np.load(pcl_dir + f"points{i:04d}.npy")
        pos = np.load(pcl_dir + f"position{i:04d}.npy") 

        t_ref = pos[:2, np.newaxis]
        phi_ref = pos[2] 

        phi_init = pos[2] + phi_sigma * np.random.randn()
        t_init = pos[:2, np.newaxis] + t_sigma * np.random.randn(2,1)
        pc = pc + pc_sigma * np.random.rand(pc.shape[0], pc.shape[1])

        R_init = rot_matrix(phi_init)
        pc = pc[:2, :]

        [R_est, t_est, add_points, corrs] = icp(pc, map, R_init, t_init)
        pc_t = np.matmul(R_est, pc) + t_est
        pc = np.matmul(R_init, pc) + t_init

        map, map_use_counts = update_map(map, map_use_counts, pc_t, add_points, corrs)
        added_points = pc_t[:, add_points]

        plt.clf()
        
        axes_t = np.matmul(R_est, axes) + t_est
        axes_t_init = np.matmul(R_init, axes) + t_init
        for axes_prev in prev_positions:
            plt.plot(axes_prev[0,:], axes_prev[1,:], c='b')
        plt.plot(axes_t_init[0,:], axes_t_init[1,:], c='r')
        plt.plot(axes_t[0,:], axes_t[1,:], c='g')

        prev_positions.append(axes_t)

        plt.scatter(pc[0,:], pc[1,:], marker='+', c='g')
        plt.scatter(map[0,:], map[1,:], c='k')
        plt.scatter(pc_t[0,:], pc_t[1,:], marker='x', c='r')

        plt.scatter(added_points[0,:], added_points[1,:], c='r')
        plt.axis('square')
        plt.draw()
        plt.pause(0.001)

        print(f"scan {i}, {pc.shape[1]} points, {len(corrs[0])} matched, {sum(add_points)} added\n")

        # phi_est = np.arccos(R_est[0,0])
        # if (abs(abs(phi_ref) - phi_est) > 10e-2 or np.any(abs(t_est - t_ref) > 10e-2)):
        #     print(f"phi diff: {abs(abs(phi_ref)-phi_est)}")
        #     print(f"t diff: {abs(t_est.T - t_ref.T)}")
        #     input()

        # input()
        # time.sleep(1)

    input()

