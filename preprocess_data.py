import numpy as np
from PIL import Image
import os
import matplotlib.pyplot as plt
from numpy.core.fromnumeric import size

IMG_TO_METERS = 1/500 # image pixel values to meters, [0..65535] -> [0..131m]
X_OFFSET = 0.79 # m
Y_OFFSET = 0 # m
Z_OFFSET = 1.73 # m
Z_LIMIT = [0.3, 3]
# Z_LIMIT = [-10, 10]

def convert_scans_to_pointcloud(img_folder, pc_folder, lidar_config, filename_base="scan"):
    if not os.path.exists(pc_folder):
        os.mkdir(pc_folder)
    for filename in sorted(os.listdir(img_folder)):
        if filename.startswith(filename_base):
            scan_img = Image.open(img_folder + filename)
            scan = np.array(scan_img)
            pc = scan_to_pointcloud(scan, lidar_config)
            pc_filename = filename.split('.')[0] + ".npy"
            np.save(pc_folder + pc_filename, pc, allow_pickle=True)
            # break
    
def scan_to_pointcloud(scan, config_file):
    f = open(config_file, 'r')
    line = f.readlines()[1].strip().split("; ")
    line = [float(x) for x in line]
    [h_size, v_size, h_start_angle, h_stop_angle] = line[:4]
    v_angles = -np.array(line[4:])*np.pi/180
    h_angles = np.linspace(h_start_angle, h_stop_angle, int(h_size))*np.pi/180
    v_angles = v_angles[:, np.newaxis]
    h_angles = h_angles[np.newaxis, :]
    r = scan*IMG_TO_METERS
    r[r==0] = np.NaN
    x = r * (np.cos(h_angles) * np.cos(v_angles)) + X_OFFSET
    y = r * (np.sin(h_angles) * np.cos(v_angles)) + Y_OFFSET
    z = r * np.sin(v_angles) + Z_OFFSET
    
    pointcloud = np.vstack([x.flatten(), y.flatten(), z.flatten()])
    pointcloud = pointcloud[:, np.logical_not(np.isnan(pointcloud[0,:]))]

    return pointcloud

if __name__ == "__main__":
    scans_dir = "data/scenario1/images/"
    pointclouds_dir = "data/scenario1/pointclouds/"
    lidar_setup_filename = "data/scenario1/img.cfg" 
    convert_scans_to_pointcloud(scans_dir, pointclouds_dir, lidar_setup_filename)

    pc = np.load(pointclouds_dir + "scan01000.npy")
    [values, bins] = np.histogram(pc[2,:], 50)

    pc = pc[:, np.logical_and(pc[2, :] > Z_LIMIT[0], pc[2, :] < Z_LIMIT[1])]

    plt.figure()
    plt.scatter(pc[0,:], pc[1,:], pc[2,:])
    
    plt.figure()
    bin_width = bins[1] - bins[0] 
    plt.bar(bins[:-1]+bin_width/2, values, 2*bin_width/3)

    plt.show()
