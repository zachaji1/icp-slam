from distutils.log import debug
from hashlib import new
import logging
import numpy as np
from scipy.spatial.distance import cdist
from copy import deepcopy

from utils import polar_dist, phi_t_delta, vecnorm, visualise_corrs

class SLAM:
    def __init__(self, debug=False, visualize=False, map_optim_period=10) -> None:
        self.map = np.zeros([2, 0])
        self.map_use_counts = np.zeros(0)
        self.debug = debug
        self.visualize = visualize
        self.sample_counter = 0
        self.map_optim_period = map_optim_period
        if debug:
            logging.basicConfig(level=logging.DEBUG)

    # Public methods:

    def get_map(self, filtered=True, threshold=5):
        if filtered:
            map = self.map[:, self.map_use_counts > threshold]
        else:
            map = self.map 
        return map

    def process_sample(self, new_points, R_init, t_init):
        new_points = new_points[:2, :]
        
        if new_points.shape[1] == 0:
            return (R_init, t_init)

        if self.map.shape[1] == 0:
            p_transformed = np.matmul(R_init, new_points) + t_init
            self.map = p_transformed
            self.map_use_counts = np.ones(self.map.shape[1])
            return (R_init, t_init)

        [R_est, t_est, add_points, corrs] = self.__icp(new_points, R_init, t_init)
        pc_t = np.matmul(R_est, new_points) + t_est
        new_points = np.matmul(R_init, new_points) + t_init

        self.__update_map(pc_t, add_points, corrs)
        
        self.sample_counter += 1

        if self.sample_counter % self.map_optim_period == 0:
            self.__optimize_map()

        return R_est, t_est

    # Private methods:

    def __icp(self, new_points, R_init, t_init, outlier_thr = 3, max_iter = 20, min_matches = 3, delta_t_min = 1e-2, delta_phi_min = 1e-3):
        R = deepcopy(R_init)
        t = deepcopy(t_init)
        d_mean_min = np.inf

        if (new_points.shape[1] == 0):
            logging.debug("ICP: No new points!")
            return (R, t, [], ([], []))

        logging.debug("ICP: Starting.")

        distances = vecnorm(new_points)

        p_transformed = np.matmul(R, new_points) + t
        [c1, c2, d_mean] = self.__match_points(self.map, p_transformed, outlier_thr)
        # [c1, c2, d_mean] = self.__match_points(self.map, p_transformed, outlier_thr*distances)
        logging.debug(f"ICP: Iteration {0}/{max_iter} - n_corrs: {len(c1)}, d_mean: {d_mean}")
        
        if self.visualize:
            visualise_corrs(c1, c2, self.map, p_transformed)

        if len(c1) < min_matches:
            R = R_init
            t = t_init
            add_points = new_points.shape[1] * [False]
            logging.debug(f"ICP: Not enough initial correspondences. {len(c1)}/{min_matches}")

            return (R, t, add_points, (c1, c2))


        i = 0
        for i in range(max_iter):
            p_map = self.map[:, c1]
            p_new = p_transformed[:, c2]
            [R_est, t_est] = self.__estimate_transform(p_map, p_new)
            R_new = np.matmul(R, R_est)
            t_new = t + t_est

            p_transformed = np.matmul(R_new, new_points) + t_new
            [c1_new, c2_new, d_mean_new] = self.__match_points(self.map, p_transformed, outlier_thr)
            # [c1_new, c2_new, d_mean_new] = self.__match_points(self.map, p_transformed, outlier_thr*distances)

            [delta_t, delta_phi] = phi_t_delta(R, R_new, t, t_new)
            logging.debug(f"ICP: Iteration {i+1}/{max_iter} - n_corrs: {len(c1_new)}, delta_t: {delta_t}, delta_phi: {delta_phi}, d_mean: {d_mean} > {d_mean_new}")

            if (len(c1_new) < min_matches):
                logging.debug(f"ICP: Not enough correspondences. {len(c1)}/{min_matches}")
                break
            
            if self.visualize:
                visualise_corrs(c1_new, c2_new, self.map, p_transformed)

            if (d_mean_new <= d_mean):
                d_mean = d_mean_new
                c1 = c1_new
                c2 = c2_new
                R = R_new
                t = t_new
            else:
                logging.debug("ICP: Error increased, stopping ICP.")
                break

            if (delta_phi < delta_phi_min and delta_t < delta_t_min):
                logging.debug("ICP: Not enough changes, stopping.")
                break

        add_points = [x not in c2 for x in range(new_points.shape[1])]

        if len(c1) < min_matches:
            R = R_init
            t = t_init
            add_points = new_points.shape[1] * [False]

        return (R, t, add_points, (c1, c2))


    def __match_points(self, points1, points2, outlier_thr):
        d_arr = cdist(points1.T, points2.T, 'euclidean')
        c1 = d_arr.argmin(0)
        c2 = d_arr.argmin(1)
        d = d_arr.min(0)
        is_mutual = c2[c1] == np.arange(0, len(c1))
        c2 = np.arange(0, len(c1))[is_mutual]   
        c1 = c1[is_mutual]
        
        d = d[is_mutual]
        # outlier_thr = outlier_thr[is_mutual]

        is_inlier = d < outlier_thr
        
        c2 = c2[is_inlier]
        c1 = c1[is_inlier]
        d_mean = d[is_inlier].mean()
        return c1, c2, d_mean


    def __estimate_transform(self, p_orig, p_new):
        mu_orig = p_orig.mean(1, keepdims=True)
        mu_new = p_new.mean(1, keepdims=True)
        W = np.matmul(p_orig - mu_orig, (p_new - mu_new).T)
        [U, _, V] = np.linalg.svd(W)
        R = np.matmul(U, V) # V.T ?
        if np.linalg.det(R) < 0:
            R = np.matmul(np.matmul(U, np.diag([1, -1])), V)
        t = mu_orig - np.matmul(R, mu_new)

        return R, t

    def __optimize_map(self, threshold = 1):
        map = self.map
        map_counts = self.map_use_counts
        n_points = map.shape[1]

        d_arr = cdist(map.T, map.T, 'euclidean')
        match_arr = d_arr < threshold
        new_indices = np.arange(n_points)
        
        for i in range(n_points-1):
            row = match_arr[i, i+1:]
            new_indices[i+1:][row] = new_indices[i]
        
        all_indices = np.arange(n_points)
        deleted_points = all_indices[all_indices != new_indices]

        for p in deleted_points:
            new_index = new_indices[p]
            map[:, new_index] = (map_counts[p] * map[:, p] + map_counts[new_index] * map[:, new_index]) / (map_counts[p] + map_counts[new_index])
            map_counts[new_index] = map_counts[p] + map_counts[new_index]
        
        map = map[:, all_indices == new_indices]
        map_counts = map_counts[all_indices == new_indices]


        new_n_points = map.shape[1]
        self.map = map
        self.map_use_counts = map_counts

        logging.debug(f"ICP: Optimized map, removed {n_points - new_n_points} ({n_points} -> {new_n_points}).")


    def __update_map(self, points, add_points, corrs):
        (c1, c2) = corrs
        new_points = points[:, add_points]
        self.map = np.hstack([self.map, new_points])
        self.map_use_counts = np.append(self.map_use_counts, np.ones(sum(add_points)))
        seen_points = points[:, c2]
        seen_use_counts = self.map_use_counts[c1]
        self.map[:, c1] = (seen_use_counts * self.map[:, c1] + seen_points) / (seen_use_counts + 1)
        self.map_use_counts[c1] += 1
