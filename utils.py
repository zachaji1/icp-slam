from matplotlib import pyplot as plt
import numpy as np

def rot_matrix(phi):
    R = np.array([[np.cos(phi), -np.sin(phi)], [np.sin(phi), np.cos(phi)]])
    return R

def pi_to_pi(angle):
    return (angle + np.pi) % (2 * np.pi) - np.pi

def R_to_phi(R):
    return np.arctan2(R[1,0], R[0,0])

def vecnorm(vec, axis=0):
    return np.sqrt((vec**2).sum(axis))

def polar_dist(points1, points2):
    points1_e = eucl_to_polar(points1)
    points2_e = eucl_to_polar(points2)
    r_diff = np.abs(points1_e[0, :, np.newaxis] - points2_e[np.newaxis, 0, :])
    phi_diff = np.tan(np.abs(points1_e[1, :, np.newaxis] - points2_e[np.newaxis, 1, :])/2)

    p_dist = r_diff + 50 * phi_diff

    return p_dist

def eucl_to_polar(points):
    r = np.sqrt(points[0,:]**2 + points[1,:]**2)
    phi = np.arctan2(points[1,:], points[0,:])
    polar = np.vstack([r, phi])

    return polar

def visualise_corrs(c1, c2, p1, p2):
    plt.cla()
    x_corrs = np.vstack([p1[0, c1], p2[0, c2]])
    y_corrs = np.vstack([p1[1, c1], p2[1, c2]])
    plot1 = plt.plot(x_corrs, y_corrs, 'ko-', label="")
    plot2 = plt.plot(p1[0, :], p1[1, :], 'r.', label="map")
    plot3 = plt.plot(p2[0, :], p2[1, :], 'b.', label="new points")
    plt.legend()
    plt.axis("equal")
    plt.draw()
    plt.show(block=False)
    input()
    

def phi_t_delta(R, R_new, t, t_new):
    delta_t = np.linalg.norm(t - t_new)
    phi = R_to_phi(R)
    phi_new = R_to_phi(R_new)
    delta_phi = np.abs(pi_to_pi(phi - phi_new))

    return delta_t, delta_phi