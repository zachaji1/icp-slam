from icp_slam_simple import SLAM
import matplotlib.pyplot as plt
import numpy as np
import os
import argparse
import importlib

from utils import rot_matrix, R_to_phi, pi_to_pi, vecnorm

ALLOWED_MODES = ["position", "orient", "pc"]
MAX_SIGMAS = [1, 0.1, 1]

def parse_inputs():
    parser = argparse.ArgumentParser(description="Evaluate SLAM instances.")
    parser.add_argument('slam_files', metavar='str', type=str, nargs='+', help='a slam instance to evaluate')
    parser.add_argument('--mode', type=str, default=['all'], nargs='+', help='mode of operation, .\
                        can be multiple of following: ["pc", "position", "orient", "all"], default=\"all\"')
    parser.add_argument('-n', '--n_samples', type=int, default=10, help='number of times the test will be run, default=10')
    parsed_input = parser.parse_args()
    # parsed_input = parser.parse_args("icp_slam_simple -n 2 --mode position".split(' '))
    
    modes = []
    if 'all' in parsed_input.mode:
        modes = ALLOWED_MODES
    else:
        for m in parsed_input.mode:
            if m.lower() in ALLOWED_MODES:
                modes.append(m.lower())
            else:
                print("Error: Mode \'{m}\' is not valid!")

    return parsed_input.slam_files, modes, parsed_input.n_samples


def import_slam_instances(slam_files, debug=False):
    slam_instances = {}
    for f in slam_files:
        try:
            module = importlib.import_module(f)
        except ModuleNotFoundError:
            print(f"Error: Module \'{f}\' not found!")
        else:
            try:
                slam = module.SLAM(debug=debug)
            except AttributeError:
                print("Error: Module \'{f}\' doesn't contain class SLAM()!")
            else:
                slam_instances[f] = slam

    return slam_instances


def generate_inputs(pcl_dir, sigmas):
    # ref_pointclouds = []
    ref_positions = []
    init_pointclouds = []
    init_positions = []

    [t_sigma, phi_sigma, pc_sigma] = sigmas
    n_measurements = int(len(os.listdir(pcl_dir))/2)

    for i in range(1, n_measurements + 1):
        pc_ref = np.load(pcl_dir + f"points{i:04d}.npy")
        pos_ref = np.load(pcl_dir + f"position{i:04d}.npy") 
        t_ref = pos_ref[:2]
        phi_ref = pos_ref[2] 

        pc_init = pc_ref + pc_sigma * np.random.rand(pc_ref.shape[0], pc_ref.shape[1])
        t_init = t_ref + t_sigma * np.random.randn(2)
        phi_init = phi_ref + phi_sigma * np.random.randn()
        pos_init = np.hstack([t_init, phi_init])

        # ref_pointclouds.append(pc_ref)
        ref_positions.append(pos_ref)
        init_pointclouds.append(pc_init)
        init_positions.append(pos_init)

    ref_positions = np.array(ref_positions).T
    init_positions = np.array(init_positions).T

    return ref_positions, init_pointclouds, init_positions


def position_orient_errors(ref_positions, ref_orient, est_positions, est_orient):
    position_errors = vecnorm(ref_positions - est_positions).mean()
    orient_errors = (np.abs(pi_to_pi(ref_orient - est_orient))).mean()    
    
    return position_errors, orient_errors


def plot_results(mode, variable, sigmas, errors, slam_files, dst_dir, save=True):
    plt.figure()
    x_labels = ["position error variance σ$_t$", "orientation error variance σ$_φ$", "point cloud error variance σ$_{pc}$"]
    y_labels = ["mean position estimate error [m]", "mean orientation estimate error [rad]"]
    x_label = x_labels[ALLOWED_MODES.index(mode)]
    y_label = y_labels[0] if (variable == 'position') else y_labels[1]

    legend = []
    for slam_f in slam_files + ['initial estimate']:
        plt.errorbar(sigmas, errors[slam_f, variable, 'mean'], yerr=errors[slam_f, variable, 'std'])
        legend.append(slam_f)
    plt.legend(legend)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    
    if save:
        filename = f"{mode}-{variable}"
        for slam_f in slam_files:
            filename += f"-{slam_f}"
        filename += ".png"
        plt.savefig(dst_dir + filename)
    else:
        plt.show()


def evaluate_slam_single(slam_instance, init_positions, init_pointclouds):
    n_measurements = len(init_pointclouds)

    est_positions = np.zeros([2, 0])
    est_orient = []

    for i in range(n_measurements):
        pc = init_pointclouds[i]
        pos = init_positions[:,i]  

        t_init = pos[:2, np.newaxis]
        phi_init = pos[2] 
        
        R_init = rot_matrix(phi_init)
        pc = pc[:2, :]

        [R_est, t_est] = slam_instance.process_sample(pc, R_init, t_init)
        phi_est = pi_to_pi(R_to_phi(R_est))

        est_orient.append(phi_est)
        est_positions = np.hstack([est_positions, t_est])

    est_orient = np.array(est_orient)

    return est_positions, est_orient


def evaluate_slam(slam_instances, slam_files, pcl_dir, mode, n_samples, dst_dir, n_sigmas = 11):
    if mode in ALLOWED_MODES:
        sigma_i = ALLOWED_MODES.index(mode)
        sigma_max = MAX_SIGMAS[sigma_i]
    else:
        print("Error: Mode \'{mode}\' is not valid!")
        return {}
    
    curr_sigmas = [0, 0, 0]
    sigmas = np.linspace(0, sigma_max, n_sigmas)

    errors_temp = {}
    errors = {}

    for slam_f in slam_files + ['initial estimate']:
        errors_temp[slam_f, 'position'] = np.zeros(n_samples)
        errors_temp[slam_f, 'orient'] = np.zeros(n_samples)
        errors[slam_f, 'position', 'mean'] = []
        errors[slam_f, 'position', 'std'] = []
        errors[slam_f, 'orient', 'mean'] = []
        errors[slam_f, 'orient', 'std'] = []

    for sigma in sigmas:
        curr_sigmas[sigma_i] = sigma
        for i in range(n_samples):
            [ref_positions, init_pointclouds, init_positions] = generate_inputs(pcl_dir, curr_sigmas)
            [init_pos_err, init_orient_err] = position_orient_errors(ref_positions[:2,:], ref_positions[2,:], init_positions[:2,:], init_positions[2,:]) 
            errors_temp['initial estimate', 'position'][i] = init_pos_err
            errors_temp['initial estimate', 'orient'][i] = init_orient_err

            for slam_f in slam_files:
                [est_positions, est_orient] = evaluate_slam_single(slam_instances[slam_f], init_positions, init_pointclouds)
                [est_pos_err, est_orient_err] = position_orient_errors(ref_positions[:2,:], ref_positions[2,:], est_positions, est_orient) 
                errors_temp[slam_f, 'position'][i] = est_pos_err
                errors_temp[slam_f, 'orient'][i] = est_orient_err

        for slam_f in slam_files + ['initial estimate']:
            errors[slam_f, 'position', 'mean'].append(errors_temp[slam_f, 'position'].mean())
            errors[slam_f, 'position', 'std'].append(errors_temp[slam_f, 'position'].std())
            errors[slam_f, 'orient', 'mean'].append(errors_temp[slam_f, 'orient'].mean())
            errors[slam_f, 'orient', 'std'].append(errors_temp[slam_f, 'orient'].std())
    
    plot_results(mode, 'position', sigmas, errors, slam_files, dst_dir)
    plot_results(mode, 'orient', sigmas, errors, slam_files, dst_dir)

    return errors


#################################################################################################

if __name__ == "__main__":
    [slam_files, modes, n_samples] = parse_inputs()
    slam_instances = import_slam_instances(slam_files, debug=False)

    # pcl_dir = "data/gen_circular_path/"
    # pcl_dir = "data/gen_track1_filter/"
    pcl_dir = "data/gen_track2/"
    dst_dir = "results/"

    plt.set_loglevel("critical")

    for mode in modes:
        evaluate_slam(slam_instances, slam_files, pcl_dir, mode, n_samples, dst_dir, n_sigmas=11)
