import rosbag
import sys
import sensor_msgs.point_cloud2 as pc2
import numpy as np

if __name__ == "__main__":
    if (len(sys.argv)) < 4:
        print("Enter bag file and topic.")
        quit()

    bag_file = sys.argv[1]
    topic = sys.argv[2]
    res_dir = sys.argv[3]

    # bag_file = "../raw_data/man1/2021-10-24-13-17-32.bag"
    # topic = "/xavier/cones/all"

    bag = rosbag.Bag(bag_file, 'r')
    
    for i, (_, msg, t) in enumerate(bag.read_messages(topics=[topic])):
        pcl_arr = []
        points = pc2.read_points(msg, field_names=["x", "y", "z"], skip_nans=True)      
        for p in points:
            pcl_arr.append(np.array(p))
        pcl_arr = np.array(pcl_arr)
        # print(pcl_arr)
        np.save(res_dir + f"points{i:04d}.npy", pcl_arr.T)